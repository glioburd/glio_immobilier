import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import 'babel-polyfill'

let $map = document.querySelector('#map')

class LeafletMap {
	constructor() {
		this.map = null
		this.bounds = []
	}
	async load(element) {
		return new Promise((resolve, reject) => {
			this.map = L.map(element, {
				scrollWheelZoom: false
			}).setView([51, -0.09], 13)
			let token =
				'pk.eyJ1IjoiZ2xpb2J1cmQiLCJhIjoiY2pwdjJia3NoMGJ0NjQyczQzcnN5ZmhqYiJ9.WUNslUSVdUuYkDBFXUfHnA'
			L.tileLayer(
				`https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=${token}`,
				{
					attribution:
						'© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
				}
			).addTo(this.map)
			resolve()
		})
	}

	addMarker(lat, lng, title) {
		let point = [lat, lng]
		this.bounds.push(point)
		return new LeafletMarker(point, title, this.map)
	}

	center() {
		this.map.fitBounds(this.bounds)
	}
}

class LeafletMarker {
	constructor(point, title, map) {
		this.title = title
		this.popup = L.popup({
			autoClose: false,
			closeOnEscapeKey: false,
			closeOnClick: false,
			closeButton: false,
			className: 'marker',
			maxWidth: 400
		})
			.setLatLng(point)
			.setContent(title)
			.openOn(map)
	}

	setActive() {
		this.popup.getElement().classList.add('is-active')
	}

	unsetActive() {
		this.popup.getElement().classList.remove('is-active')
	}

	addEventListener(event, cb) {
		// this.popup.addEventListener('add', () => {
			this.popup.getElement().addEventListener(event, cb)
		// })
	}

	setContent(text) {
		this.popup.setContent(text)
		this.popup.getElement().classList.add('is-expanded')
		this.popup.update()
	}

	resetContent() {
		this.popup.setContent(this.title)
		this.popup.getElement().classList.remove('is-expanded')
		this.popup.update()
	}
}

const initMap = async function() {
	let map = new LeafletMap()
	let hoverMarker = null
	let activeMarker = null
	await map.load($map)
	Array.from(document.querySelectorAll('.js-marker')).forEach(item => {
		let marker = map.addMarker(
			item.dataset.lat,
			item.dataset.lng,
			item.dataset.title
		)
		item.addEventListener('mouseover', function() {
			if (hoverMarker !== null) {
				hoverMarker.unsetActive()
			}
			marker.setActive()
			hoverMarker = marker
		})
		item.addEventListener('mouseleave', function() {
			if (hoverMarker !== null) {
				hoverMarker.unsetActive()
			}
		})
		marker.addEventListener('click', function() {
			if (activeMarker !== null) {
				activeMarker.resetContent()
			}
			marker.setContent(item.innerHTML.trim())
			activeMarker = marker
		})
	})
	map.center()
}

if ($map !== null) {
	initMap()
}
