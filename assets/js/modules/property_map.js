import L from 'leaflet'
import 'leaflet/dist/leaflet.css'

export default class LeafletMap {
    static init () {
        let map = document.querySelector('#map')

        if (map === null) {
            return
        }

        let blueIcon = L.icon({
            iconUrl: '/images/marker-icon.png'
        })
        let center = [map.dataset.lat, map.dataset.lng]
        map = L.map('map').setView(center, 13)
        let token = 'pk.eyJ1IjoiZ2xpb2J1cmQiLCJhIjoiY2pwdjJia3NoMGJ0NjQyczQzcnN5ZmhqYiJ9.WUNslUSVdUuYkDBFXUfHnA'
        L.tileLayer(`https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=${token}`, {
            maxZoom: 18,
            minZoom: 12,
            attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map)
        L.marker(center, {icon: blueIcon}).addTo(map)
    }

    addMarker (lat, lng, text) {

    }
}

LeafletMap.init()