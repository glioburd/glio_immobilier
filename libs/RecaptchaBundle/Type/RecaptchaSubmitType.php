<?php

namespace Glioburd\RecaptchaBundle\Type;

use Glioburd\RecaptchaBundle\Constraints\Recaptcha;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class RecaptchaSubmitType extends AbstractType
{
    /**
     * @var string
     */
    private $key;

    /**
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //Ce champs là n'est pas relié à une information dans notre objet qui représente les données
            'mapped' => false,
            'constraints' => new Recaptcha()
        ]);
    }

    /**
     * Donner un prefix à notre bloc
     * Exemple : prefix 'submit', ça utilisera le submitType
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'recaptcha_submit';
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     * @return void
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['label'] = false;
        $view->vars['key'] = $this->key;
        $view->vars['button'] = $options['label'];
    }

    /**
     * Quel est le champs qui est parent à celui-là : ça héritera des mêmes options
     *
     * @return void
     */
    public function getParent()
    {
        return TextType::class;
    }
}