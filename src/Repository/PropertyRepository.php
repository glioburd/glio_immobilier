<?php

namespace App\Repository;

use App\Entity\Property;
use App\Entity\Picture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\QueryBuilder;
use App\Entity\PropertySearch;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(RegistryInterface $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Property::class);
        $this->paginator = $paginator;
    }

    /**
     * Retourne tous les biens non vendus
     *
     * @return PaginationInterface
     */
    public function paginateAllVisible(PropertySearch $search, int $page): PaginationInterface
    {
        $query = $this->findVisibleQuery();

        // where écrase le dernier where. addWhere pour en ajouter d'autres
        if ($search->getMaxPrice()) {
            $query = $query
                ->andWhere('p.price <= :maxprice')
                ->setParameter('maxprice', $search->getMaxPrice());
        }

        if ($search->getMinSurface()) {
            $query = $query
                ->andWhere('p.surface >= :minsurface')
                ->setParameter('minsurface', $search->getMinSurface());
        }

        if ($search->getLat() && $search->getLng() && $search->getDistance()) {
            $query = $query
                ->andWhere('(6353 * 2 * ASIN(SQRT( POWER(SIN((p.lat - :lat) *  pi()/180 / 2), 2) +COS(p.lat * pi()/180) * COS(:lat * pi()/180) * POWER(SIN((p.lng -:lng) * pi()/180 / 2), 2) ))) <= :distance')
                ->setParameter('lng', $search->getLng())
                ->setParameter('lat', $search->getLat())
                ->setParameter('distance', $search->getDistance())
                ;
        }

        if ($search->getOptions()->count() > 0) {
            $key = 0;
            foreach ($search->getOptions() as $option) {
                $query = $query
                ->andWhere(":option$key MEMBER OF p.options")
                ->setParameter("option$key", $option);  
            }
        }

        $properties = $this->paginator->paginate(
            $query->getQuery(), // Query, not result
            $page,          	// page number
            12					// limit per page
        );
        
        $this->hydratePicture($properties);
        return $properties;
    }

    /**
     * Retourne les 4 derniers biens
     *
     * @return Property[]
     */
    public function findLatest() : array
    {
        $properties = $this->findVisibleQuery()
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
        $this->hydratePicture($properties);
        return $properties;
    }

    /**
     * Retourne les biens non vendus
     *
     * @return QueryBuilder
     */
    private function findVisibleQuery() : QueryBuilder
    {
        return $this->createQueryBuilder('p')
            // ->select('p', 'pics')
            // ->leftJoin('p.pictures', 'pics')
            ->where('p.sold = false');
    }

    private function hydratePicture($properties)
    {
        // Pour être sûr d'avoir une collection (ce qui n'est pas le cas dans home)
        if (method_exists($properties, 'getItems')) {
            $properties = $properties->getItems();
        }

        $pictures = $this->getEntityManager()->getRepository(Picture::class)->findForProperties($properties);

        foreach ($properties as $property) {
            /** @var $property Property */
            if ($pictures->containsKey($property->getId())) {
                $property->setPicture($pictures->get($property->getId()));
            }
        }
    }
}
