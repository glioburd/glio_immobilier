<?php

namespace App\Controller;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Notification\ContactNotification;
use App\Entity\Option;

class PropertyController extends AbstractController
{
    /**
     * @var PropertyRepository
     */
	private $repository;
	
	/**
     * @var ObjectManager
     */
    private $em;
    
    public function __construct(PropertyRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }
    /**
     * @Route("/biens", name="property.index")
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);
        //$properties = $this->repository->findAllVisible();
        return $this->render('property/index.html.twig', [
            'current_menu'  => 'properties',
            'properties'    => $this->repository->paginateAllVisible($search, $request->query->getInt('page', 1)),
            'form'          => $form->createView()
        ]);
    }

    /**
     * Page d'un bien
     * Soit on passe en paramètre $id et $slug et on fait un $property = $this->repository->find($id); dans la fonction
     * Soit on met en paramètre un objet Property, il trouve tout seul le bien depuis l'annotation de la route depuis l'id
     * @Route("/biens/{slug}-{id}", name="property.show", requirements = {"slug": "[a-z0-9\-]*"})
     * @param Property $property
     * @param [string] $slug
     * @return Response
     */

    public function show(Property $property, string $slug, Request $request, ContactNotification $notification): Response
    {
        if ($property->getSlug() !== $slug) {
            return $this->redirectToRoute('property.show', [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ], 301);
        }

        $contact = new Contact();
        $contact->setProperty($property);
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $notification->notify($contact);
            $this->addFlash('success', 'Votre email a bien été envoyé');
            return $this->redirectToRoute('property.show', [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ]);
        }
        return $this->render('property/show.html.twig', [
            'property' => $property,
            'current_menu' => 'properties',
            'form' => $form->createView()
        ]);
    }
}
