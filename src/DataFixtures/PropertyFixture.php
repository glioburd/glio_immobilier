<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Property;
use App\Entity\Option;
use Faker\Factory;

class PropertyFixture extends Fixture
{

    private $faker;
    private $options;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create('fr_FR');
        $this->addOptions($manager);
        $this->addProperties($manager);
    }

    public function addOptions($manager)
    {
        $option = new Option;
        $option->setName('Accès handicapé');
        $manager->persist($option);
        $this->options[] = $option;

        $option = new Option;
        $option->setName('Ascenseur');
        $manager->persist($option);
        $this->options[] = $option;

        $option = new Option;
        $option->setName('Jardin');
        $manager->persist($option);
        $this->options[] = $option;

        $manager->flush();
    }

    public function addProperties(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 100; $i++) {
            $property = new Property;
            $property
                ->setTitle($faker->words(3, true))
                ->setDescription($faker->sentence(30, true))
                ->setSurface($faker->numberBetween(20, 350))
                ->setRooms($faker->numberBetween(2, 10))
                ->setBedrooms($faker->numberBetween(1, 9))
                ->setFloor($faker->numberBetween(0, 15))
                ->setPrice($faker->numberBetween(100000, 1000000))
                ->setHeat($faker->numberBetween(0, count(Property::HEAT) - 1))
                ->setCity($faker->city)
                ->setAddress($faker->address)
                ->setPostalCode($faker->postcode)
                ->setSold(false)
                ->setLat($faker->latitude($min = 42.3327778, $max = 51.0891667))
                ->setLng($faker->longitude($min = -4.795555555555556, $max = 8.230555555555556));

            $randomNumberOfOptions = rand(0, sizeof($this->options));
            if ($randomNumberOfOptions > 1) {
                $randomKeys = array_rand($this->options, $randomNumberOfOptions);
                foreach ($randomKeys as &$option) {
                    $option = $this->options[$option];
                    $property->addOption($option);
                }
            } elseif ($randomNumberOfOptions === 1) {
                $option = $this->options[array_rand($this->options, $randomNumberOfOptions)];
                $property->addOption($option);
            }
            $manager->persist($property);
            $manager->flush();
        }
    }
}
