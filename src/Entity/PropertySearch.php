<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

class PropertySearch
{
    /**
     * Prix maximum
     * @var int|null
     * @Assert\Range(min=1)
     */
    private $maxPrice;

    /**
     * Surface minimum
     *
     * @var int|null
     * @Assert\Range(min=10, max=400)
     */
    private $minSurface;

    /**
     * Options
     *
     * @var ArrayCollection
     */
    private $options;

    /**
     * @var float|null
     */
    private $lat;

    /**
     * @var float|null
     */
    private $lng;

    /**
     * @var integer|null
     */
    private $distance;

    /**
     * @var string|null
     */
    private $address;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    /**
     * Get prix maximum
     *
     * @return  int|null
     */ 
    public function getMaxPrice()
    {
        return $this->maxPrice;
    }

    /**
     * Set prix maximum
     *
     * @param  int|null  $maxPrice  Prix maximum
     *
     * @return  self
     */ 
    public function setMaxPrice(int $maxPrice): self
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    /**
     * Get surface minimum
     *
     * @return  int|null
     */ 
    public function getMinSurface(): ?int
    {
        return $this->minSurface;
    }

    /**
     * Set surface minimum
     *
     * @param  int|null  $minSurface  Surface minimum
     *
     * @return self
     */ 
    public function setMinSurface(int $minSurface): self
    {
        $this->minSurface = $minSurface;

        return $this;
    }

    /**
     * Get options
     *
     * @return  ArrayCollection
     */ 
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param ArrayCollection $options
     */ 
    public function setOptions(ArrayCollection $options)
    {
        $this->options = $options;
    }

    /**
     * Get the value of lat
     *
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * Set the value of lat
     * @param float|null $lat  
     * @return self
     */
    public function setLat($lat): self
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get the value of lng
     *
     * @return float|null
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * Set the value of lng
     * @param float|null $lng  
     * @return self
     */
    public function setLng($lng): self
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * Get the value of distance
     *
     * @return integer|null
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }

    /**
     * Set the value of distance
     * @param integer|null  $distance  
     * @return self
     */
    public function setDistance($distance): self
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * Get the value of address
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set the value of address
     * @param string|null $address  
     * @return self
     */
    public function setAddress($address): self
    {
        $this->address = $address;
        return $this;
    }
}
